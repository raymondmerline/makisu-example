The following environment variables need to be set, otherwise it's fork and go:

`DOCKERHUB_USERNAME`

`DOCKERHUB_PASSWORD`

`DOCKERHUB_ORG`
